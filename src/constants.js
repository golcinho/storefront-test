export const COMPANY = 'Hero store';

export const IMAGES_ROOT = '/media/';

export const LOGO_URL = IMAGES_ROOT + 'logo.png';
export const DEFAULT_HERO_IMAGE_URL = IMAGES_ROOT + 'plates-header.jpg';

export const PATHS = {
  NOT_FOUND: '/404',
  HOME: '/',
  PRODUCT_DETAILS: '/products/:details',
  CART: '/cart',
};

export const URLS = {
  GET_PRODUCTS: '/products.json',
};

export const TOASTER_CONFIG = {
  hideProgressBar: true,
  autoClose: 3000,
  closeOnClick: true,
};