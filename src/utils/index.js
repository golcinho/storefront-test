import { IMAGES_ROOT } from '../constants';
import { currencyFormatter } from '../helpers/CurrencyFormatter';

const REPLACE_SPACE_BY_REGEX = /\s+/g; // Replace spaces by -
// eslint-disable-next-line
const REMOVE_NON_WORD_REGEX = /[^\w\-]+/g; // Remove all non-word chars
// eslint-disable-next-line
const REPLACE_MULTIPLE_DASH_REGEX = /\-\-+/g; // Replace multiple - by single -
const TRIM_START_REGEX = /^-+/; // Trim - from start of text
const TRIM_END_REGEX = /-+$/; // Trim - from end of text

export const getImageUrl = image => image && IMAGES_ROOT + image;

export const getProductUrl = slug => `products/${slug}`;

export const getSlugFromTitle = title => title
  && title.toString().toLowerCase()
    .replace(REPLACE_SPACE_BY_REGEX, '-')
    .replace(REMOVE_NON_WORD_REGEX, '')
    .replace(REPLACE_MULTIPLE_DASH_REGEX, '-')
    .replace(TRIM_START_REGEX, '')
    .replace(TRIM_END_REGEX, '');

export const normalizeProductsData = products => {
  let result = {};
  products.forEach(product => {
    const slug = getSlugFromTitle(product.title);
    result[slug] = {
      ...product,
      formattedPrice: currencyFormatter(product.price),
      imageUrl: getImageUrl(product.image),
      slug,
    };
  });
  return result;
};
