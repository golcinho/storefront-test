import { getImageUrl, getProductUrl, getSlugFromTitle, normalizeProductsData } from './index';

describe('getImageUrl util', () => {
  it('should return correct image path', () => {
    const imageUrl = getImageUrl('heme.jpg');
    expect(imageUrl).toEqual('/media/heme.jpg');
  })
});

describe('getProductUrl util', () => {
  it('should return correct product url', () => {
    const imageUrl = getProductUrl('heme');
    expect(imageUrl).toEqual('products/heme');
  })
});

describe('getSlugFromTitle util', () => {
  it('should return slug without spaces from start to text', () => {
    const slug = getSlugFromTitle('    hello world');
    expect(slug).toEqual('hello-world');
  });

  it('should return slug without spaces from end to text', () => {
    const slug = getSlugFromTitle('hello world      ');
    expect(slug).toEqual('hello-world');
  });

  it('should return slug where spaces replaced by -', () => {
    const slug = getSlugFromTitle('Hello world one more time');
    expect(slug).toEqual('hello-world-one-more-time');
  });

  it('should return slug with all characters is lower case', () => {
    const slug = getSlugFromTitle('HeLlO WoRlD');
    expect(slug).toEqual('hello-world');
  });

  it('should return slug without whitespaces around the dash', () => {
    const slug = getSlugFromTitle('Hello - World');
    expect(slug).toEqual('hello-world');
  });

  it('should return slug without multiple dashes', () => {
    const slug = getSlugFromTitle('hello --- world - one ---more --- time')
    expect(slug).toEqual('hello-world-one-more-time');
  });

  it('should return slug without non-word characters', () => {
    const slug = getSlugFromTitle('**&&Hello World#$% o*&ne more (#&%Ti)#!me');
    expect(slug).toEqual('hello-world-one-more-time');
  })
});

describe('normalizeProductsData util', () => {
  const data = require('../../public/products');
  const normalizedData = normalizeProductsData(data);

  it('should return object instead array', () => {
    expect(typeof normalizedData === 'object' && normalizedData.constructor === Object).toBe(true);
  });

  it('should return an object where every keys are equal to slug', () => {
    const slug = Object.values(normalizedData)[0].slug;
    const key = Object.keys(normalizedData)[0];
    expect(key).toEqual(slug);
  });
});
