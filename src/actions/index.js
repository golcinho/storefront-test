export default {
  GET_PRODUCTS_START: 'GET_PRODUCTS_START',
  GET_PRODUCTS_SUCCESS: 'GET_PRODUCTS_SUCCESS',
  GET_PRODUCTS_FAIL: 'GET_PRODUCTS_FAIL',

  ADD_PRODUCT_TO_CART: 'ADD_PRODUCT_TO_CART',
  REMOVE_PRODUCT_FROM_CART: 'REMOVE_PRODUCT_FROM_CART',

  INCREASE_PRODUCT_QUANTITY: 'INCREASE_PRODUCT_QUANTITY',
  DECREASE_PRODUCT_QUANTITY: 'DECREASE_PRODUCT_QUANTITY',
};