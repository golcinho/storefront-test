import ACTIONS from './index';
import { toast } from 'react-toastify'

export const addProductToCart = (productSlug, quantity = 1) => {
  toast('Product successfully added!');

  return {
    type: ACTIONS.ADD_PRODUCT_TO_CART,
    productSlug,
    quantity,
  };
};

export const removeProductFromCart = productSlug => ({
  type: ACTIONS.REMOVE_PRODUCT_FROM_CART,
  productSlug,
});

export const decreaseProductQuantity = productSlug => ({
  type: ACTIONS.DECREASE_PRODUCT_QUANTITY,
  productSlug,
});

export const increaseProductQuantity = productSlug => ({
  type: ACTIONS.INCREASE_PRODUCT_QUANTITY,
  productSlug,
});