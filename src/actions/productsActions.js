import { getJSON } from '../helpers/API';
import { URLS } from '../constants';
import ACTIONS from './index';

const getProductsStart = () => ({
  type: ACTIONS.GET_PRODUCTS_START,
});

const getProductsFail = () => ({
  type: ACTIONS.GET_PRODUCTS_FAIL,
});

const getProductsSuccess = products => ({
  type: ACTIONS.GET_PRODUCTS_SUCCESS,
  products,
});

export const getProducts = forceUpdate => (dispatch, getState) => {
  const { isLoading, isError, data } = getState().products;

  if (!isLoading && !isError && Object.keys(data).length && !forceUpdate) {
    return;
  }

  dispatch(getProductsStart());

  return getJSON(URLS.GET_PRODUCTS)
    .then(products => {
      return dispatch(getProductsSuccess(products));
    })
    .catch(() => {
      return dispatch(getProductsFail());
    });
};