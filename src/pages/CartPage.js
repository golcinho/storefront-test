import React, { Fragment } from 'react';
import Cart from '../containers/Cart';

const CartPage = props => (
  <Fragment>
    <Cart {...props} />
  </Fragment>
);

export default CartPage;