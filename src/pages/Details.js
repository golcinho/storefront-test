import React, { Fragment } from 'react';
import ProductDetails from '../containers/ProductDetails';

const Details = props => (
  <Fragment>
    <ProductDetails {...props} />
  </Fragment>
);

export default Details;