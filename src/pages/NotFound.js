import React, { Fragment } from 'react';

const NotFound = () => (
  <Fragment>
    <div className="container container--strict">
      <h1>404 PAGE NOT FOUND :(</h1>
    </div>
  </Fragment>
);

export default NotFound;