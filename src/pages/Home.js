import React, { Fragment } from 'react';
import { HeroSection } from '../components';
import Products from '../containers/Products';

const Home = () => (
  <Fragment>
    <HeroSection
      sectionTitle="Plates"
      sectionDescription="Plates description"
    />
    <Products />
  </Fragment>
);

export default Home;