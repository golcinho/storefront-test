import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { ScrollToTop } from '../components';
import { PATHS } from '../constants';

import Home from '../pages/Home';
import Details from '../pages/Details';
import CartPage from '../pages/CartPage';
import NotFound from '../pages/NotFound';

class Routes extends Component {
  render() {
    return (
      <ScrollToTop>
        <Switch>
          <Route exact path={PATHS.HOME} component={Home} />
          <Route exact path={PATHS.PRODUCT_DETAILS} component={Details} />
          <Route exact path={PATHS.CART} component={CartPage} />
          <Route component={NotFound} />
        </Switch>
      </ScrollToTop>
    );
  }
}

export default Routes;