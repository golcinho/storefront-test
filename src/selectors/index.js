import { createSelector } from 'reselect';
import { currencyFormatter } from '../helpers/CurrencyFormatter';

const selectCartProducts = state => state.cart;

const selectProducts = state => state.products.data;

export const selectQuantityOfProductsInCart = createSelector(
  selectCartProducts,
  products => Object.keys(products).length,
);

export const selectProductsFromCart = createSelector(
  [selectProducts, selectCartProducts],
  (products, cardProducts) =>
    Object.keys(cardProducts).map(slug => {
      const quantity = cardProducts[slug].quantity;
      const totalPrice = products[slug].price * quantity;

      return {
        ...products[slug],
        quantity: cardProducts[slug].quantity,
        totalPrice: totalPrice,
        formattedTotalPrice: currencyFormatter(totalPrice),
      };
    })
);

export const selectCartTotal = createSelector(
  selectProductsFromCart,
  products => products.reduce((acc, product) =>
    acc += product.totalPrice, 0)
);