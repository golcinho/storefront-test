import ACTIONS from '../actions/index';
import { normalizeProductsData } from '../utils';

const initialState = {
  data: {},
  isLoading: null,
  isError: null,
};

const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.GET_PRODUCTS_START:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case ACTIONS.GET_PRODUCTS_FAIL:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    case ACTIONS.GET_PRODUCTS_SUCCESS:
      return {
        ...state,
        data: normalizeProductsData(action.products),
        isLoading: false,
        isError: false,
      };
    default:
      return state;
  }
};

export default productsReducer;