import ACTIONS from '../actions/index';

const cartReducer = (state = {}, action) => {
  switch (action.type) {
    case ACTIONS.ADD_PRODUCT_TO_CART: {
      const slug = action.productSlug;
      const quantity = action.quantity;

      return {
        ...state,
        [slug]: {
          quantity: state[slug]
            ? state[slug].quantity + quantity
            : quantity,
        }
      };
    }
    case ACTIONS.INCREASE_PRODUCT_QUANTITY: {
      const slug = action.productSlug;
      const currentQuantity = state[slug].quantity;

      if (currentQuantity - 1 > 0) {
        return {
          ...state,
          [slug]: {
            quantity: currentQuantity - 1,
          }
        }
      }
      return state;
    }
    case ACTIONS.DECREASE_PRODUCT_QUANTITY:
      return {
        ...state,
        [action.productSlug]: {
          quantity: state[action.productSlug].quantity + 1,
        }
      };
    case ACTIONS.REMOVE_PRODUCT_FROM_CART:
      delete state[action.productSlug];
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default cartReducer;