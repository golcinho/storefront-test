import React, { Fragment } from 'react';
import './index.css';
import { DEFAULT_HERO_IMAGE_URL } from '../../constants';
import { getImageUrl } from '../../utils';

const getBackgroundImageStyle = image => ({
  backgroundImage: `url(${image ? getImageUrl(image) : DEFAULT_HERO_IMAGE_URL})`
});

const HeroSection = ({
  sectionTitle = false,
  sectionDescription = false,
  sectionImage = false,
  ...props,
}) => {

  return (
    <section
      className="hero"
      style={getBackgroundImageStyle(sectionImage)}
      {...props}
    >
      <div className="hero__content clear-fix">
        {sectionTitle &&
          <Fragment>
            <h1>{sectionTitle}</h1>
            <hr className="hero__separator" />
          </Fragment>
        }
        {sectionDescription &&
          <p>{sectionDescription}</p>
        }
      </div>
    </section>
  );
};

export default HeroSection;