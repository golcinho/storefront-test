import React from 'react';
import { Link } from 'react-router-dom';
import { getProductUrl } from '../../utils';
import './index.css';

const Thumbnail = ({
  item,
  onAddToCardClick,
  direction = 'column',
  showActions = true,
  ...props,
}) => {
  const {
    title,
    brand,
    formattedPrice,
    imageUrl,
    slug,
  } = item;
  const isColumn = direction === 'column';
  const directionClassName = isColumn
    ? 'thumbnail'
    : 'thumbnail thumbnail--row';

  return (
    <div className={directionClassName} {...props}>
      {imageUrl &&
        <div className="thumbnail__image">
          <img src={imageUrl} alt={title} />
          {showActions &&
            <div className="thumbnail__backdrop">
              <Link className="btn btn--block btn--primary" to={getProductUrl(slug)}>View Details</Link>
              <button
                className="btn btn--block btn--primary"
                onClick={() => onAddToCardClick(slug)}
              >
                Add to Card
              </button>
            </div>
          }
        </div>
      }
      <div className="thumbnail__body">
        {brand &&
          <div className="thumbnail__meta">{brand}</div>
        }
        {title &&
          <Link to={getProductUrl(slug)} className="thumbnail__title">
            <span>{title}</span>
          </Link>
        }
        {formattedPrice && isColumn &&
          <div className="thumbnail__meta">{formattedPrice}</div>
        }
      </div>
    </div>
  );
};

export default Thumbnail;