import React, { Component } from 'react';
import './index.css';

class Table extends Component {
  renderTableHeader = () => {
    const { header } = this.props;

    return (
      <thead>
        <tr>
          {header.map((columnTitle, key) => (
            <th key={key}>{columnTitle}</th>
          ))}
        </tr>
      </thead>
    );
  };

  renderTableColumns = () => {
    const { columns } = this.props;

    return (
      <tbody>
        {columns.map((column, key) => (
          <tr key={key}>
            {column.map((columnContent, key) => (
              <td key={key}>{columnContent}</td>
            ))}
          </tr>
        ))}
      </tbody>
    );
  };

  render() {
    const { columns } = this.props;

    if (!columns || columns.length === 0) {
      return (
        <p className="text-center">Sorry, table is empty... Please try to add some products.</p>
      )
    }

    return (
      <div className="table">
        <table>
          {this.renderTableHeader()}
          {this.renderTableColumns()}
        </table>
      </div>
    );
  };
}

export default Table;