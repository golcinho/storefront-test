import React, { Component } from 'react';
import './index.css';

const BUTTON_TYPES = {
  INCREMENT: 'increment',
  DECREMENT: 'decrement',
};

class FieldNumber extends Component {
  state = {
    count: this.props.startCount || 1,
  };

  handleActionsClick = (e, type) => {
    const {onChangeCallBack} = this.props;

    e.preventDefault();
    const { count } = this.state;

    if (count === 1 && type === BUTTON_TYPES.INCREMENT) {
      return;
    }

    this.setState({
      count: type === BUTTON_TYPES.DECREMENT ? count + 1 : count - 1,
    });

    onChangeCallBack && onChangeCallBack(type);
  };

  render() {
    return (
      <div className="number">
        <div className="number__result">
          {this.state.count}
        </div>
        <div className="number__actions">
          <button onClick={e => this.handleActionsClick(e, BUTTON_TYPES.DECREMENT)}>+</button>
          <button onClick={e => this.handleActionsClick(e, BUTTON_TYPES.INCREMENT)}>-</button>
        </div>
      </div>
    );
  };
}

export default FieldNumber;