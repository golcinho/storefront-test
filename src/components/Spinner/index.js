import React from 'react';
import './index.css';

const Spinner = props => (
  <div className="spinner" {...props}>
    <div className="spinner__animation">
    </div>
  </div>
);

export default Spinner;