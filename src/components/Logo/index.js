import React from 'react';
import { Link } from 'react-router-dom';
import { COMPANY, LOGO_URL, PATHS } from '../../constants';
import './index.css';

const Logo = ({
  linkTo = PATHS.HOME,
  imgUrl = LOGO_URL,
  description = COMPANY,
  ...props,
}) => (
  <Link className="logo" to={linkTo} {...props}>
    <img className="logo__image" src={imgUrl} alt={description} />
  </Link>
);

export default Logo;