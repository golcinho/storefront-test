import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getProducts } from '../actions/productsActions';
import { Spinner } from '../components';

export default WrappedComponent => {
  class WithProducts extends Component {
    componentDidMount() {
      const { dispatch } = this.props;
      dispatch(getProducts());
    };

    render() {
      const { isError, isLoading, products } = this.props;

      if (isLoading) {
        return (
          <div className="container container--strict">
            <Spinner />
          </div>
        );
      }

      if (isError || products.length === 0) {
        return (
          <div className="container container--strict">
            <h2>Ops.. No result found!</h2>
          </div>
        );
      }

      return <WrappedComponent {...this.props} />
    };
  }

  const mapStateToProps = state => ({
    products: Object.values(state.products.data),
    isLoading: state.products.isLoading,
    isError: state.products.isError,
  });

  return connect(mapStateToProps)(WithProducts);
};

