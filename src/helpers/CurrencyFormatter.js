const LOCALE = 'en-US';
const STYLE = 'currency';
const CURRENCY = 'USD';

const formatterWithCoins = new Intl.NumberFormat(LOCALE, {
  style: STYLE,
  currency: CURRENCY,
  minimumFractionDigits: 2,
});


export const currencyFormatter = (value = '',) => {
  const numberValue = Number(value);
  const isNegativeValue = !!numberValue && Math.sign(numberValue) === -1;
  const positiveValue = Math.abs(numberValue);

  const result = formatterWithCoins.format(positiveValue);

  return isNegativeValue ? `-${result}` : result;
};