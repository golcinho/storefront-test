import { currencyFormatter } from './CurrencyFormatter';

describe('currencyFormatter()', () => {
  it('should return the number in format $10,300,000.00 when value is a number', () => {
    const currency = currencyFormatter(10300000);
    expect(currency).toEqual('$10,300,000.00');
  });

  it('should return the number in format -$10,300,000.00 when value is a number', () => {
    const currency = currencyFormatter(-10300000);
    expect(currency).toEqual('-$10,300,000.00');
  });

  it('should return the number in format -$10,300,000.00 when value is a string', () => {
    const currency = currencyFormatter('-103000.00');
    expect(currency).toEqual('-$103,000.00');
  });

  it('should return the number in format $10,300,000.00 when value is a string', () => {
    const currency = currencyFormatter('10300000');
    expect(currency).toEqual('$10,300,000.00');
  });

  it('should return $ when value is empty', () => {
    const currency = currencyFormatter();
    expect(currency).toEqual('$0.00');
  });
});