const CACHE_NO_STORE = 'no-store';
const CONTENT_TYPE = 'Content-Type';
const CONTENT_TYPE_JSON = 'application/json';
const SAME_ORIGIN = 'same-origin';

export const getJSON = url =>
  fetch(url, {
    cache: CACHE_NO_STORE,
    credentials: SAME_ORIGIN,
    headers: new Headers({
      [CONTENT_TYPE]: CONTENT_TYPE_JSON,
    }),
    mode: SAME_ORIGIN,
  }).then(response => response.json());