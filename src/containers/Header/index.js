import React from 'react';
import { connect } from 'react-redux';
import { selectQuantityOfProductsInCart } from '../../selectors';
import { Logo } from '../../components';
import { Link } from 'react-router-dom';
import { PATHS } from '../../constants';
import './index.css';

const Header = ({
  quantity,
}) => (
  <header className="header">
    <Logo />
    <Link to={PATHS.CART} className="btn">
      My Cart {!!quantity && `(${quantity})`}
    </Link>
  </header>
);

const mapStateToProps = state => ({
  quantity: selectQuantityOfProductsInCart(state),
});

export default connect(mapStateToProps)(Header);