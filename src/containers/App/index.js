import React, { Component, Fragment } from 'react';
import { ToastContainer } from 'react-toastify';
import { TOASTER_CONFIG } from '../../constants';
import Header from '../Header';
import Routes from '../../routes';
import './index.css';

class Index extends Component {
  render() {
    return (
      <Fragment>
        <div className="layout">
          <Header />
          <Routes />
        </div>
        <ToastContainer {...TOASTER_CONFIG} />
      </Fragment>
    );
  }
}

export default Index;
