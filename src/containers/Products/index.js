import React, { Component } from 'react';
import { addProductToCart } from '../../actions/cartActions';
import { Thumbnail } from '../../components';
import withProducts from '../../hoc/withProducts';
import './index.css';

class Products extends Component {
  handleAddItemToCart = slug => {
    const { dispatch } = this.props;
    return dispatch(addProductToCart(slug));
  };

  render() {
    const { products } = this.props;

    return (
      <div className="products container container--strict">
        <div className="row row--md">
          {products.map((product, key) =>
            <div key={key} className="col col--xs-12 col--sm-6 col--md-4">
              <Thumbnail
                item={product}
                onAddToCardClick={this.handleAddItemToCart}
              />
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default withProducts(Products);