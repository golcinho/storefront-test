import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { PATHS } from '../../constants';
import withProducts from '../../hoc/withProducts';
import { selectCartTotal, selectProductsFromCart } from '../../selectors';
import {
  decreaseProductQuantity,
  increaseProductQuantity,
  removeProductFromCart
} from '../../actions/cartActions';
import {
  FieldNumber,
  Table,
  Thumbnail,
} from '../../components';
import './index.css';

const initialTableHeader = [
  'Product',
  'Quantity',
  'Total',
  'Actions',
];

class Cart extends Component {
  handleRemoveItem = slug => {
    const { dispatch } = this.props;
    dispatch(removeProductFromCart(slug));
  };

  handleChangeItemQuantity = slug => type => {
    const { dispatch } = this.props;

    if (type === 'decrement') {
      return dispatch(decreaseProductQuantity(slug))
    }

    return dispatch(increaseProductQuantity(slug))
  };

  buildTableColumns = () => {
    const { products } = this.props;

    return products.map(product => [
      <Thumbnail
        direction="row"
        showActions={false}
        item={product}
      />,
      <FieldNumber
        startCount={product.quantity}
        onChangeCallBack={this.handleChangeItemQuantity(product.slug)}
      />,
      <span className="cart__price">{product.formattedTotalPrice}</span>,
      <button
        className="btn btn--block"
        onClick={() => this.handleRemoveItem(product.slug)}
      >
        x
      </button>
    ]);
  };

  render() {
    const { cartTotal } = this.props;
    return (
      <div className="cart">
        <h1 className="text-center">Shopping Cart</h1>
        <div className="container container--strict">
          <div className="cart__section">
            <Table
              header={initialTableHeader}
              columns={this.buildTableColumns()}
            />
            {!!cartTotal &&
              <p className="text-right">
                <strong>Total: {cartTotal}</strong>
              </p>
            }
            <div className="text-center">
              <Link className="btn btn--primary" to={PATHS.HOME}>Continue Shopping</Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: selectProductsFromCart(state),
  cartTotal: selectCartTotal(state),
});

export default compose(
  withProducts,
  connect(mapStateToProps),
)(Cart);