import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { PATHS } from '../../constants';
import withProducts from '../../hoc/withProducts';
import { addProductToCart } from '../../actions/cartActions';
import { FieldNumber } from '../../components';
import "./index.css";

class ProductDetails extends Component {
  fieldNumberRef = React.createRef();

  handleAddItemToCart = slug => {
    const { dispatch } = this.props;
    return dispatch(addProductToCart(
      slug,
      this.fieldNumberRef.current.state.count,
    ));
  };

  render() {
    const { product } = this.props;

    if (!product) {
      return <Redirect to={PATHS.NOT_FOUND} />
    }

    return (
      <div className="product-details container container--strict">
        <div className="row row--middle row--lg">
          <div className="col col__lg--6">
            <img
              className="product-details__image"
              src={product.imageUrl}
              alt={product.title} />
          </div>
          <div className="col col__lg--6">
            <div className="product-details__meta">
              {product.brand}
            </div>
            <h1 className="product-details__title">
              {product.title}
            </h1>
            <div className="product-details__price">
              {product.formattedPrice}
            </div>
            <div className="product-details__description">
              <p>{product.description}</p>
            </div>
            <hr className="product-details__separator" />
            <div className="row row--sm row--middle row--center">
              <div className="col col--self-width">
                <FieldNumber ref={this.fieldNumberRef} />
              </div>
              <div className="col col--self-width">
                <button
                  onClick={() => this.handleAddItemToCart(product.slug)}
                  className="btn btn--primary btn--lg nowrap"
                >
                  Add to Card
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };
}

const mapStateToProps = (state, ownProps) => ({
  product: state.products.data[ownProps.match.params.details],
});

export default compose(
  connect(mapStateToProps),
  withProducts,
)(ProductDetails);