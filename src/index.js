import React from 'react';
import ReactDOM from 'react-dom';
import Root from './containers/Root';
import './assets/styles/index.css';

ReactDOM.render(
    <Root />,
    document.getElementById('root')
);
